//
//  PlacesInfoViewModel.swift
//  Lahore
//
//  Created by Huda Jawed on 7/29/19.
//  Copyright © 2019 Huda Jawed. All rights reserved.
//

import UIKit
import Firebase
import FirebaseDatabase

protocol dataDelegate {
    
    func didRecievedData(data:[InfoDataModel])
    func didRecieveFavorites(favourites:[String])
    func didRecieveRatings(ratings:[Dictionary<String,Any>])
    func didFailed()
}

class InfoViewModel {
    
    let imagesArray = [
        "Rock City Resort" : [#imageLiteral(resourceName: "rock_city1"),#imageLiteral(resourceName: "rock_city2") , #imageLiteral(resourceName: "rock_city3"),#imageLiteral(resourceName: "rock_city4")] ,
        "Shelton's Rezidor Hotel" : [#imageLiteral(resourceName: "Shelton's Rezidor Hotel1") , #imageLiteral(resourceName: "Shelton's Rezidor Hotel2") , #imageLiteral(resourceName: "Shelton's Rezidor Hotel3")],
        "Podina Restaurant" : [#imageLiteral(resourceName: "Podina Restaurant1"),#imageLiteral(resourceName: "Podina Restaurant2") , #imageLiteral(resourceName: "Podina Restaurant3")],
        "Jarogo Waterfall" : [#imageLiteral(resourceName: "Jarogo Waterfall1"),#imageLiteral(resourceName: "Jarogo Waterfall2"),#imageLiteral(resourceName: "Jarogo Waterfall3")],
        "Mahodand Lake" : [#imageLiteral(resourceName: "Mahodand Lake1"),#imageLiteral(resourceName: "Mahodand Lake2") ,#imageLiteral(resourceName: "Mahodand Lake3")] ,
        "Serena Hotel" : [#imageLiteral(resourceName: "Serena Hotel1") , #imageLiteral(resourceName: "Serena Hotel2") , #imageLiteral(resourceName: "Serena Hotel3")] ,
        "Swat Continental Hotel" : [#imageLiteral(resourceName: "Swat Continental Hotel1") , #imageLiteral(resourceName: "Swat Continental Hotel2") , #imageLiteral(resourceName: "Swat Continental Hotel3")] ,
        "Pearl Continental Hotel" : [#imageLiteral(resourceName: "Pearl Continental Hotel1") , #imageLiteral(resourceName: "Pearl Continental Hotel2") , #imageLiteral(resourceName: "Pearl Continental Hotel3")] ,
        "G Qurban Restaurant" : [#imageLiteral(resourceName: "G Qurban Restaurant1") , #imageLiteral(resourceName: "G Qurban Restaurant2") , #imageLiteral(resourceName: "G Qurban Restaurant3")] ,
        "Shingrai Waterfall" : [#imageLiteral(resourceName: "Shingrai Waterfall1") , #imageLiteral(resourceName: "Shingrai Waterfall2") , #imageLiteral(resourceName: "Shingrai Waterfall3")] ,
        "Kundol Lake" : [#imageLiteral(resourceName: "Kundol Lake1") , #imageLiteral(resourceName: "Kundol Lake2") , #imageLiteral(resourceName: "Kundol Lake3")] ,
        "Daral Lake" : [#imageLiteral(resourceName: "Daral Lake1") , #imageLiteral(resourceName: "Daral Lake2") , #imageLiteral(resourceName: "Daral Lake3")] ,
        "Bashigram Lake" : [#imageLiteral(resourceName: "Bashigram Lake1") , #imageLiteral(resourceName: "Bashigram Lake2") , #imageLiteral(resourceName: "Bashigram Lake3")] ,
        "Pari Lake" : [#imageLiteral(resourceName: "Pari Lake1") , #imageLiteral(resourceName: "Pari Lake2") , #imageLiteral(resourceName: "Pari Lake3")] ,
        "Burjal Swat Hotel" : [#imageLiteral(resourceName: "Burjal Swat Hotel1") , #imageLiteral(resourceName: "Burjal Swat Hotel2") , #imageLiteral(resourceName: "Burjal Swat Hotel3")] ,
        "Hotel Hills City" : [#imageLiteral(resourceName: "Hotel Hills City1") , #imageLiteral(resourceName: "Hotel Hills City2") , #imageLiteral(resourceName: "Hotel Hills City3")] ,
        "Cafe Akbari" : [#imageLiteral(resourceName: "cafe akbari1") , #imageLiteral(resourceName: "cafe akbari2") ]
        
    ]
    
    var delegate : dataDelegate?
    var ratingDelegate : RatingsDelegate?
    
    //MARK:- Hotels Fetch
    
    func fetchHotelsData() {
        
        var dataModelArray = [InfoDataModel]()
        let ref = Database.database().reference()
        ref.child("hotels").observeSingleEvent(of: .value) { (snapshot) in
            
            let dataDic = snapshot.value as? Dictionary<String,Any>
            for singleData in dataDic!
            {
                let data = singleData.value as? Dictionary<String,String>
                let item = InfoDataModel(placeName: singleData.key, placeDetail: data!["detail"]!, placeImages: self.imagesArray[singleData.key]!, placeLocation: data!["location"]!, isFavourites: data!["favourites"]! , ratings: data!["ratings"]!)
                dataModelArray.append(item)
            }
            self.delegate?.didRecievedData(data: dataModelArray)
        }
    }
    
    //MARK:- Restaurants Fetch
    
    func fetchRestaurantsData() {
        
        var dataModelArray = [InfoDataModel]()
        let ref = Database.database().reference()
        ref.child("restaurants").observeSingleEvent(of: .value) { (snapshot) in
            
            let dataDic = snapshot.value as? Dictionary<String,Any>
            for singleData in dataDic!
            {
                let data = singleData.value as? Dictionary<String,String>
                let item = InfoDataModel(placeName: singleData.key, placeDetail: data!["detail"]!, placeImages: self.imagesArray[singleData.key]!, placeLocation: data!["location"]!, isFavourites: data!["favourites"]! , ratings: data!["ratings"]!)
                dataModelArray.append(item)
            }
            self.delegate?.didRecievedData(data: dataModelArray)
        }
    }
    
    //MARK:- WaterFall Fetch
    
    func fetchWaterFallsData() {
        
        var dataModelArray = [InfoDataModel]()
        let ref = Database.database().reference()
        ref.child("waterFall").observeSingleEvent(of: .value) { (snapshot) in
            
            let dataDic = snapshot.value as? Dictionary<String,Any>
            for singleData in dataDic!
            {
                let data = singleData.value as? Dictionary<String,String>
                let item = InfoDataModel(placeName: singleData.key, placeDetail: data!["detail"]!, placeImages: self.imagesArray[singleData.key]!, placeLocation: data!["location"]!, isFavourites: data!["favourites"]! , ratings: data!["ratings"]!)
                dataModelArray.append(item)
            }
            self.delegate?.didRecievedData(data: dataModelArray)
        }
    }
    
    //MARK:- Lakes Fetch
    
    func fetchLakesData() {
        
        var dataModelArray = [InfoDataModel]()
        let ref = Database.database().reference()
        ref.child("lakes").observeSingleEvent(of: .value) { (snapshot) in
            
            let dataDic = snapshot.value as? Dictionary<String,Any>
            for singleData in dataDic!
            {
                let data = singleData.value as? Dictionary<String,String>
                let item = InfoDataModel(placeName: singleData.key, placeDetail: data!["detail"]!, placeImages: self.imagesArray[singleData.key]!, placeLocation: data!["location"]!, isFavourites: data!["favourites"]! , ratings: data!["ratings"]!)
                dataModelArray.append(item)
            }
            self.delegate?.didRecievedData(data: dataModelArray)
        }
    }
    
    func getFavourites() {
        
        guard let userid = UserDefaults.standard.object(forKey: "userid") as? String else {
            return
        }
        
        var favourites = [String]()
        let ref = Database.database().reference()
        ref.child("favorites").observeSingleEvent(of: .value) { (snapshot) in
            
            if let dataDic = snapshot.value as? Dictionary<String,Any>
            {
                for singleData in dataDic
                {
                    if singleData.key == Auth.auth().currentUser?.uid
                    {
                        let data = singleData.value as? Dictionary<String,String>
                        for item in data!
                        {
                            favourites.append(item.key)
                        }
                    }
                }
                self.delegate?.didRecieveFavorites(favourites: favourites)
            }
        }
    }
    
    
    func getRatings(place:String) {
        
        let ref = Database.database().reference()
        ref.child("ratings").observeSingleEvent(of: .value) { (snapshot) in
            
            if let dataDic = snapshot.value as? Dictionary<String,Any>
            {
                for singleData in dataDic
                {
                    var one : Double = 0
                    var two : Double = 0
                    var three : Double = 0
                    var four : Double = 0
                    var five : Double = 0
                    
                    if singleData.key == place
                    {
                        let data = singleData.value as? Dictionary<String,Any>
                        
                        for item in data!
                        {
                            if let singleRating = item.value as? Double
                            {
                                if singleRating == 1
                                {
                                    one = one + 1
                                }
                                else if singleRating == 2
                                {
                                    two = two + 1
                                }
                                else if singleRating == 3
                                {
                                    three = three + 1
                                }
                                else if singleRating == 4
                                {
                                    four = four + 1
                                }
                                else if singleRating == 5
                                {
                                    five = five + 1
                                }
                            }
                        }
                        
                        let rate = (one*1)+(two*2)+(three*3)+(four*4)+(five*5)
                        let total = (one+two+three+four+five)
                        
                        if total == 0.0 {
                            self.ratingDelegate?.didRecieveRatings(ratings: 0.0)
                            return
                        }
                        
                        let rating : Double = rate/total
                        self.ratingDelegate?.didRecieveRatings(ratings: rating)
                        break
                    }
                }
                
            }
        }
    }
    
    func getMyRatings() {
        
        guard let userid = UserDefaults.standard.object(forKey: "userid") as? String else {
            return
        }
        
        var ratingsList = [Dictionary<String,Any>]()
            let ref = Database.database().reference()
            ref.child("ratings").observeSingleEvent(of: .value) { (snapshot) in
                
                // Name & rating
                if let dataDic = snapshot.value as? Dictionary<String,Any> {
                    
                    for singleData in dataDic {
                        
                            let data = singleData.value as? Dictionary<String,Any>
                        
                            for item in data!
                            {
                                if item.key == userid
                                {
                                   var dic = Dictionary<String,Any>()
                                    dic["placeName"] = singleData.key
                                    dic["ratings"] = item.value
                                    ratingsList.append(dic)
                                }
                                
                            }
                        
                    }
                    self.delegate?.didRecieveRatings(ratings: ratingsList)
                }
            }
        }
        
    }
    
    struct InfoDataModel {
        
        var placeName: String?
        var placeDetail: String?
        var placeImages: [UIImage]?
        var placeLocation: String?
        var isFavourites: String?
        var ratings: String?
        
        init(placeName:String,placeDetail:String,placeImages:[UIImage],placeLocation:String,isFavourites:String,ratings:String)
        {
            self.placeName = placeName
            self.placeDetail = placeDetail
            self.placeImages = placeImages
            self.placeLocation = placeLocation
            self.isFavourites = isFavourites
            self.ratings = ratings
        }
}

