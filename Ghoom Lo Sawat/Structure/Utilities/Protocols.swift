//
//  Protocol.swift
//  HireMile
//
//  Created by mac on 5/14/18.
//  Copyright © 2018 mac. All rights reserved.
//

import Foundation
import UIKit

protocol RefreshDelegate {
    func StartRefresh()
}


protocol AlertViewDelegate {
    func okAction()
}

protocol HeartDelegate {
    func action()
}


protocol AlertViewDelegateAction {
    func okButtonAction()
    func cancelAction()
}

protocol FavoritesDelegate {
    func updateFavorites()
}


