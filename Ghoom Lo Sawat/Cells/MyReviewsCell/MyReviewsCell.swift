//
//  MyReviewsCell.swift
//  Ghoom Lo Sawat
//
//  Created by Abdul Muqeem on 07/08/2019.
//  Copyright © 2019 Abdul Muqeem. All rights reserved.
//

import UIKit
import Cosmos

class MyReviewsCell: UITableViewCell {

    @IBOutlet weak var lblTitle:UILabel!
    @IBOutlet weak var rating:CosmosView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
}
