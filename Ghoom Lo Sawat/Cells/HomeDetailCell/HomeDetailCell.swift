//
//  HomeDetailCell.swift
//  Ghoom Lo Sawat
//
//  Created by Abdul Muqeem on 07/08/2019.
//  Copyright © 2019 Abdul Muqeem. All rights reserved.
//

import UIKit
import Cosmos

class HomeDetailCell: UITableViewCell {

    @IBOutlet weak var imgBanner:UIImageView!
    @IBOutlet weak var imgHeart:UIImageView!
    @IBOutlet weak var lblTitle:UILabel!
    @IBOutlet weak var lblDescription:UILabel!
    @IBOutlet weak var lblReadMore:UILabel!
    @IBOutlet weak var rating:CosmosView!
    
    var delegate:HeartDelegate?
    var isFavourite:Bool = false

    override func awakeFromNib() {
        super.awakeFromNib()
        
    }
    
    @IBAction func heartAction(_ sender : UIButton) {
        return
    }
    
}
