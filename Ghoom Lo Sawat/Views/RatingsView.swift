//
//  RatingsView.swift
//  Lahore
//
//  Created by Huda Jawed on 8/3/19.
//  Copyright © 2019 Huda Jawed. All rights reserved.
//

import UIKit
import Cosmos

protocol RatingsDelegate {
    
    func doneRating(ratings:Double)
    func cancelBtn()
    func didRecieveRatings(ratings: Double)
}

class RatingsView: UIView {
    
    var delegate : RatingsDelegate?

    @IBOutlet weak var ratingsView: CosmosView!
    
    override init(frame: CGRect) {
        super.init(frame: frame)
    }
    
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
   
    @IBAction func didTapDoneBtn(_ sender: Any)
    {
        self.delegate?.doneRating(ratings:ratingsView.rating)
    }
    
    @IBAction func didTapCancelBtn(_ sender: Any)
    {
        self.delegate?.cancelBtn()
    }
}
