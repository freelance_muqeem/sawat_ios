//
//  DetailViewController.swift
//  Ghoom Lo Sawat
//
//  Created by Abdul Muqeem on 07/08/2019.
//  Copyright © 2019 Abdul Muqeem. All rights reserved.
//

import UIKit
import ImageSlideshow
import Firebase
import FirebaseDatabase
import Cosmos
import STZPopupView

class DetailViewController: UIViewController {
    
    class func instantiateFromStoryboard() -> DetailViewController {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        return storyboard.instantiateViewController(withIdentifier: String(describing: self)) as! DetailViewController
    }
    
    @IBOutlet weak var imgSlider:ImageSlideshow!
    @IBOutlet weak var imgHeart:UIImageView!
    @IBOutlet weak var lblText:UILabel!
    @IBOutlet weak var rating:CosmosView!
    
    var dataModel = InfoViewModel()
    var obj:InfoDataModel!
    var imageArray:[ImageSource]? = [ImageSource]()
    var type:String! = ""
    
    var isFavourite:Bool = false
    var favorites = [String]()
    var delegate : FavoritesDelegate?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if let title = obj.placeName {
            self.title = title
        }
        
        self.PlaceNavigationButtons(selectorForLeft:  #selector(backAction) , leftImage: BACK_IMAGE , selectorForRight:  #selector(shareAction) , rightImage: SHARE_IMAGE )
        self.navigationController?.ShowNavigationBar()
        self.navigationController?.ThemedNavigationBar()
        self.navigationController?.ChangeTitleColor(color: .white)
        self.navigationController?.ChangeTitleFont()
        
        if self.type == "Hotels" {
            self.type = "hotels"
        }
        else if self.type == "Restaurants" {
            self.type = "restaurants"
        }
        else if self.type == "Water Falls" {
            self.type = "waterFall"
        }
        else if self.type == "Lakes" {
            self.type = "lakes"
        }
        
        if let description = obj.placeDetail {
            self.lblText.text = description
        }
        
        if self.obj.isFavourites == "true" {
            self.imgHeart.image = UIImage(named: "heart_red")
        } else {
            self.imgHeart.image = UIImage(named: "favourites_icon")
        }
        
        //Image Slider
        if let image = obj.placeImages {
            
            for img in image {
                let obj = ImageSource(image: img)
                self.imageArray?.append(obj)
            }
            
            self.imgSlider.setImageInputs(self.imageArray!)
            self.imgSlider.contentScaleMode = .scaleAspectFill
            self.imgSlider.slideshowInterval = 1.5
            self.imgSlider.zoomEnabled = true
            
        }
        
        self.dataModel.getRatings(place: self.obj.placeName!)
        self.favorites.contains(self.obj.placeName!)
        self.dataModel.ratingDelegate = self
        
    }
    
    @objc func backAction() {
        self.navigationController?.popViewController(animated: true)
    }
    
    @objc func shareAction() {
        
        var objectsToShare = [String]()
        objectsToShare.append("App Name: \nSawat Tour")
        objectsToShare.append("Place Name: \n\(self.obj.placeName!)")
        objectsToShare.append("Place Detail: \n\(self.obj.placeDetail!)")
        objectsToShare.append(self.obj.placeLocation!)
        
        let activityViewController = UIActivityViewController(activityItems: objectsToShare, applicationActivities: nil)
        activityViewController.popoverPresentationController?.sourceView = self.view
        present(activityViewController, animated: true, completion: nil)
        
    }
    
    @IBAction func locationAction(_ sender : UIButton) {
        
        let vc = MapViewController.instantiateFromStoryboard()
        vc.obj = self.obj
        self.navigationController?.pushViewController(vc, animated: true)
        
    }
    
    @IBAction func ratingAction(_ sender : UIButton) {
        
        let popupView = Bundle.main.loadNibNamed("RatingsView", owner: self, options: nil)?[0] as! RatingsView
        popupView.delegate = self
        self.presentPopupView(popupView)
        
    }
    
    @IBAction func heartAction(_ sender : UIButton) {
        
        if self.obj.isFavourites == "false" {
            self.markFavorites()
            self.imgHeart.image = UIImage(named: "heart_red")
            self.isFavourite = true
        } else {
            self.unmarkFavorites()
            self.imgHeart.image = UIImage(named: "favourites_icon")
            self.isFavourite = false
        }
    }
    
    func unmarkFavorites() {
        
        guard let uid = UserDefaults.standard.object(forKey: "userid") as? String else {
            self.showBanner(title: "Error", subTitle: "No Network connection", style: .danger)
            self.stopLoading()
            return
        }
        
        self.startLoading(message: "")
        Database.database().reference().child("favorites").child(uid).child(self.obj.placeName!).removeValue
            { error,arg  in
                
                if error != nil
                {
                    self.stopLoading()
                    self.showBanner(title: "Error", subTitle: error!.localizedDescription, style: .danger)
                    return
                }
                
                self.stopLoading()
                self.updateFavourites()
                self.showBanner(title: "Success", subTitle: "Unmarked from favorites", style: .success)
                self.delegate?.updateFavorites()
                for i in 0..<self.favorites.count
                {
                    if self.obj.placeName! == self.favorites[i]
                    {
                        self.favorites.remove(at: i)
                        break
                    }
                }
        }
        
        DispatchQueue.main.asyncAfter(deadline: .now()+4.0) {
            self.stopLoading()
        }
        
    }
    
    func markFavorites() {
        
        self.startLoading(message: "")
        guard let userid = UserDefaults.standard.object(forKey: "userid") as? String else {
            self.showBanner(title: "Error", subTitle: "No Network connection", style: .danger)
            self.stopLoading()
            return
        }
        
        Database.database().reference().child("favorites").child(userid).updateChildValues([self.obj.placeName!:self.type!], withCompletionBlock: { (error, ref) in
            
            if let error = error
            {
                self.stopLoading()
                self.showBanner(title: "Error", subTitle: error.localizedDescription, style: .danger)
                return
            }
            
            self.stopLoading()
            self.updateFavourites()
            self.showBanner(title: "Success", subTitle: "Marked to favorites", style: .success)
            self.delegate?.updateFavorites()
            self.favorites.append(self.obj.placeName!)
            
        })
        
        DispatchQueue.main.asyncAfter(deadline: .now()+4.0) {
            self.stopLoading()
        }
    }
}

extension DetailViewController : RatingsDelegate {
    
    func cancelBtn(){
        self.dismissPopupView()
    }
    
    func didRecieveRatings(ratings: Double) {
        self.rating.rating = ratings
        self.updateRating()
        print(ratings)
    }
    
    func doneRating(ratings:Double) {
        
        if ratings == 0 {
            self.showBanner(title: "Alert", subTitle: "Rating can't be zero", style: .danger)
            return
        }
        
        self.startLoading(message: "")
        
        guard let userid = UserDefaults.standard.object(forKey: "userid") as? String else {
            self.showBanner(title: "Error", subTitle: "No Network connection", style: .danger)
            self.stopLoading()
            return
        }
        
        Database.database().reference().child("ratings").child(self.obj.placeName!).updateChildValues([userid : ratings], withCompletionBlock: { (error, ref) in
            
            if let error = error
            {
                self.stopLoading()
                self.showBanner(title: "Error", subTitle: error.localizedDescription, style: .danger)
                return
            }
            
            self.stopLoading()
            self.showBanner(title: "Success", subTitle: "Rating has been added", style: .success)
            self.dataModel.getRatings(place: self.obj.placeName!)
            self.dismissPopupView()
            
        })
        
        DispatchQueue.main.asyncAfter(deadline: .now()+4.0) {
            self.stopLoading()
        }
        
    }
    
    func updateRating() {
        
        self.startLoading(message: "")
        
        guard let userid = UserDefaults.standard.object(forKey: "userid") as? String else {
            self.showBanner(title: "Error", subTitle: "No Network connection", style: .danger)
            self.stopLoading()
            return
        }
        
        Database.database().reference().child(self.type!).child(self.obj.placeName!).updateChildValues(["ratings":String(self.rating.rating)], withCompletionBlock: { (error, ref) in
            
            if let error = error
            {
                self.stopLoading()
                self.showBanner(title: "Error", subTitle: error.localizedDescription, style: .danger)
                return
            }
            
            self.stopLoading()
            
        })
        
        DispatchQueue.main.asyncAfter(deadline: .now()+4.0) {
            self.stopLoading()
        }
    }
    
    func updateFavourites() {
        
        self.startLoading(message: "")
        
        guard let userid = UserDefaults.standard.object(forKey: "userid") as? String else {
            self.showBanner(title: "Error", subTitle: "No Network connection", style: .danger)
            self.stopLoading()
            return
        }
        
        var value = ""
        if self.isFavourite == true {
            value = "true"
        } else {
            value = "false"
        }
    Database.database().reference().child(self.type!).child(self.obj.placeName!).updateChildValues(["favourites":value], withCompletionBlock: { (error, ref) in
            
            if let error = error
            {
                self.stopLoading()
                self.showBanner(title: "Error", subTitle: error.localizedDescription, style: .danger)
                return
            }
            
            self.stopLoading()
            
        })
        
        DispatchQueue.main.asyncAfter(deadline: .now()+4.0) {
            self.stopLoading()
        }
    }
    
}






