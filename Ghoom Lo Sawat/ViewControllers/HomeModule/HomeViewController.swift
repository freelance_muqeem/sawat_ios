//
//  HomeViewController.swift
//  Ghoom Lo Sawat
//
//  Created by Abdul Muqeem on 06/08/2019.
//  Copyright © 2019 Abdul Muqeem. All rights reserved.
//

import UIKit

class HomeViewController: UIViewController {
    
    class func instantiateFromStoryboard() -> HomeViewController {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        return storyboard.instantiateViewController(withIdentifier: String(describing: self)) as! HomeViewController
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.title = "SAWAT"
        self.PlaceLeftButton(image: MENU_IMAGE , selector: #selector(menuAction))
        self.navigationController?.ThemedNavigationBar()
        self.navigationController?.ChangeTitleColor(color: .white)
        self.navigationController?.ChangeTitleFont()
        
        if TYPE == "Register" {
            self.showBanner(title: "Congratulations!", subTitle: "Your account has been created successfully", style: .success)
            TYPE = ""
        }
        else if TYPE == "Login" {
            self.showBanner(title: "Welcome!", subTitle: "You are login successfully", style: .success)
            TYPE = ""
        }
    }
    
    @objc func menuAction() {
        self.showLeftViewAnimated(self)
    }
    
    @IBAction func action(_ sender:UIButton) {
        
        if sender.tag == 1 {
            let vc = HomeDetailViewController.instantiateFromStoryboard()
            vc.type = "Hotels"
            self.navigationController?.pushViewController(vc, animated: true)
        }
        else if sender.tag == 2 {
            let vc = HomeDetailViewController.instantiateFromStoryboard()
            vc.type = "Restaurants"
            self.navigationController?.pushViewController(vc, animated: true)
        }
        else if sender.tag == 3 {
            let vc = HomeDetailViewController.instantiateFromStoryboard()
            vc.type = "Water Falls"
            self.navigationController?.pushViewController(vc, animated: true)
        }
        else if sender.tag == 4 {
            let vc = HomeDetailViewController.instantiateFromStoryboard()
            vc.type = "Lakes"
            self.navigationController?.pushViewController(vc, animated: true)
        }
        
    }
}

