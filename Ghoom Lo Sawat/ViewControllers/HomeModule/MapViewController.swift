//
//  MapViewController.swift
//  Ghoom Lo Sawat
//
//  Created by Abdul Muqeem on 07/08/2019.
//  Copyright © 2019 Abdul Muqeem. All rights reserved.
//

import UIKit
import MapKit

class MapViewController: UIViewController , MKMapViewDelegate {
    
    class func instantiateFromStoryboard() -> MapViewController {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        return storyboard.instantiateViewController(withIdentifier: String(describing: self)) as! MapViewController
    }
    
    @IBOutlet weak var mapView:MKMapView!
    
    let regionRadius: CLLocationDistance = 1000
    var initialLocation : CLLocation!
    
    var obj:InfoDataModel!
    var latitude:Double! = 0.0
    var longitude:Double! = 0.0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.title = self.obj.placeName!
        self.PlaceLeftButton(image: BACK_IMAGE , selector: #selector(backAction))
        self.navigationController?.ShowNavigationBar()
        self.navigationController?.ThemedNavigationBar()
        self.navigationController?.ChangeTitleColor(color: .white)
        self.navigationController?.ChangeTitleFont()

        let array = self.obj.placeLocation!.split(separator: ",")
        if let lat = array.first {
            self.latitude = Double(lat)
        }
        if let long = array.last {
            self.longitude = Double(long)
        }
        
        // Show Pin on Map
        let location = MKPointAnnotation()
        location.title = self.obj.placeName!
        location.coordinate = CLLocationCoordinate2D(latitude: self.latitude! , longitude: self.longitude!)
        self.mapView.addAnnotation(location)
        
        self.initialLocation = CLLocation(latitude: self.latitude! , longitude: self.longitude!)
        self.centerMapOnLocation(location: initialLocation)
        
    }
    
    @objc func backAction() {
        self.navigationController?.popViewController(animated: true)
    }
    
    func centerMapOnLocation(location: CLLocation) {
        
        let coordinateRegion = MKCoordinateRegion(center: location.coordinate,
                                                  latitudinalMeters: regionRadius, longitudinalMeters: regionRadius)
        mapView.setRegion(coordinateRegion, animated: true)
    }

}
