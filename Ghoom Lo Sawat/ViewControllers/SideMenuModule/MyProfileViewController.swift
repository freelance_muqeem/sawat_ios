//
//  MyProfileViewController.swift
//  Ghoom Lo Sawat
//
//  Created by Abdul Muqeem on 07/08/2019.
//  Copyright © 2019 Abdul Muqeem. All rights reserved.
//

import UIKit
import Firebase
import FirebaseDatabase

class MyProfileViewController: UIViewController {

    class func instantiateFromStoryboard() -> MyProfileViewController {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        return storyboard.instantiateViewController(withIdentifier: String(describing: self)) as! MyProfileViewController
    }
    
    @IBOutlet weak var txtFullName:UITextField!
    @IBOutlet weak var txtEmail:UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.title = "My Profile"
        self.PlaceLeftButton(image: MENU_IMAGE , selector: #selector(menuAction))
        self.navigationController?.ThemedNavigationBar()
        self.navigationController?.ChangeTitleColor(color: .white)
        self.navigationController?.ChangeTitleFont()
        
        if let name = UserDefaults.standard.object(forKey: "username") as? String {
            self.txtFullName.text = name
        }
        
        if let email = UserDefaults.standard.object(forKey: "email") as? String {
            self.txtEmail.text = email
        }
    }
    
    @objc func menuAction() {
        self.showLeftViewAnimated(self)
    }
    
    @IBAction func updateAction(_ sender : UIButton) {
        
        self.view.endEditing(true)
        let fullName = self.txtFullName.text!
        
        if fullName.isEmptyOrWhitespace() {
            self.showBanner(title: "Error", subTitle: "Please enter full name" , style: .danger)
            return
        }
        
        self.startLoading(message: "")
        
        guard let userid = UserDefaults.standard.object(forKey: "userid") as? String else {
            self.showBanner(title: "Error", subTitle: "No Network connection", style: .danger)
            self.stopLoading()
            return
        }
        
        Database.database().reference().child("users").child(userid).updateChildValues(["username":fullName], withCompletionBlock: { (error, ref) in
            
            if let error = error
            {
                self.stopLoading()
                self.showBanner(title: "Error", subTitle: error.localizedDescription, style: .danger)
                return
            }
            
            self.stopLoading()
            self.showBanner(title: "Success", subTitle: "Full Name has been updated" , style: .success)
            
            let ref = Database.database().reference()
            ref.child("users").observeSingleEvent(of: .value, with:
                { (data) in
                    
                    let user = data.value as? Dictionary<String,Any>
                    if let userData = user?[(Auth.auth().currentUser?.uid)!] as? Dictionary<String,Any>
                    {
                        if let name = userData["username"] as? String {
                            UserDefaults.standard.set(name, forKey: "username")
                        }
                        
                        if let email = userData["email"] as? String {
                            UserDefaults.standard.set(email, forKey: "email")
                        }
                        
                    }
            })
            
        })
        
        DispatchQueue.main.asyncAfter(deadline: .now()+4.0) {
            self.stopLoading()
        }
    }
}
