//
//  MyFavouritesViewController.swift
//  Ghoom Lo Sawat
//
//  Created by Abdul Muqeem on 07/08/2019.
//  Copyright © 2019 Abdul Muqeem. All rights reserved.
//

import UIKit

class MyFavouritesViewController: UIViewController {
    
    class func instantiateFromStoryboard() -> MyFavouritesViewController {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        return storyboard.instantiateViewController(withIdentifier: String(describing: self)) as! MyFavouritesViewController
    }
    
    @IBOutlet weak var tblView:UITableView!
    
    let dataModel = InfoViewModel()
    
    var hotelsDataArray = [InfoDataModel]()
    var allHotelsData = [InfoDataModel]()
    
    var restaurantDataArray = [InfoDataModel]()
    var allRestaurantsData = [InfoDataModel]()
    
    var waterFallDataArray = [InfoDataModel]()
    var allwaterFallData = [InfoDataModel]()
    
    var lakesDataArray = [InfoDataModel]()
    var allLakesData = [InfoDataModel]()
    
    var FavouritesDataArray = [InfoDataModel]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.title = "My Favourites"
        self.PlaceLeftButton(image: MENU_IMAGE , selector: #selector(menuAction))
        self.navigationController?.ThemedNavigationBar()
        self.navigationController?.ChangeTitleColor(color: .white)
        self.navigationController?.ChangeTitleFont()
        
        self.dataModel.delegate = self
        
        //Register Cell
        let cell = UINib(nibName:String(describing:HomeDetailCell.self), bundle: nil)
        self.tblView.register(cell, forCellReuseIdentifier: String(describing: HomeDetailCell.self))
    
        self.startLoading(message: "")
        self.dataModel.fetchHotelsData()
        self.dataModel.fetchRestaurantsData()
        self.dataModel.fetchWaterFallsData()
        self.dataModel.fetchLakesData()
        self.stopLoading()
        
    }
    
    @objc func menuAction() {
        self.showLeftViewAnimated(self)
    }
    
}


extension MyFavouritesViewController : UITableViewDelegate , UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.FavouritesDataArray.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return GISTUtility.convertToRatio(310)
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let obj:InfoDataModel! = self.FavouritesDataArray[indexPath.row]
        
        let cell:HomeDetailCell = tableView.dequeueReusableCell(withIdentifier:String(describing:HomeDetailCell.self)) as!  HomeDetailCell
        
        if let title = obj.placeName {
            cell.lblTitle.text = title
        }
        
        if let description = obj.placeDetail {
            cell.lblDescription.text = description
        }
        
        if let image = obj.placeImages!.first {
            cell.imgBanner.image = image
        }
        
        if let ratings = obj.ratings {
            cell.rating.rating = ratings.toDouble()!
        }
        
        cell.lblReadMore.isHidden = true
        cell.imgHeart.image = UIImage(named: "heart_red")
        
        return cell
    }
}

extension MyFavouritesViewController: dataDelegate {
    
    func didRecieveRatings(ratings: [Dictionary<String,Any>]){
        
    }
    
    
    func didRecievedData(data: [InfoDataModel]) {
        
        for favourites in data {
            if favourites.isFavourites == "true" {
                self.FavouritesDataArray.append(favourites)
            }
        }
        
        self.FavouritesDataArray = self.FavouritesDataArray.sorted { $0.placeName! < $1.placeName! }
        self.tblView.reloadData()
        self.stopLoading()
        
    }
    
    func didRecieveFavorites(favourites: [String]){
        
    }
    
    
    func didFailed() {
        
        self.showBanner(title: "Error", subTitle: "Failed to fetch data", style: .danger)
        self.stopLoading()
    }
    
}

