//
//  AboutUsViewController.swift
//  Ghoom Lo Sawat
//
//  Created by Abdul Muqeem on 07/08/2019.
//  Copyright © 2019 Abdul Muqeem. All rights reserved.
//

import UIKit

class AboutUsViewController: UIViewController {

    class func instantiateFromStoryboard() -> AboutUsViewController {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        return storyboard.instantiateViewController(withIdentifier: String(describing: self)) as! AboutUsViewController
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.title = "About US"
        self.PlaceLeftButton(image: BACK_IMAGE , selector: #selector(backAction))
        self.navigationController?.ThemedNavigationBar()
        self.navigationController?.ChangeTitleColor(color: .white)
        self.navigationController?.ChangeTitleFont()
        
    }
    
    @objc func backAction() {
        self.navigationController?.popViewController(animated: true)
    }

}
