//
//  FeedbackViewController.swift
//  Ghoom Lo Sawat
//
//  Created by Abdul Muqeem on 07/08/2019.
//  Copyright © 2019 Abdul Muqeem. All rights reserved.
//

import UIKit
import Firebase
import FirebaseDatabase

class FeedbackViewController: UIViewController {

    class func instantiateFromStoryboard() -> FeedbackViewController {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        return storyboard.instantiateViewController(withIdentifier: String(describing: self)) as! FeedbackViewController
    }
    
    @IBOutlet weak var txtFullName:UITextField!
    @IBOutlet weak var txtEmail:UITextField!
    @IBOutlet weak var txtDescription:UITextView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.title = "Feedback Form"
        self.PlaceLeftButton(image: BACK_IMAGE , selector: #selector(backAction))
        self.navigationController?.ThemedNavigationBar()
        self.navigationController?.ChangeTitleColor(color: .white)
        self.navigationController?.ChangeTitleFont()
        
        if let name = UserDefaults.standard.object(forKey: "username") as? String {
            self.txtFullName.text = name
        }
        
        if let email = UserDefaults.standard.object(forKey: "email") as? String {
            self.txtEmail.text = email
        }
        
        self.txtDescription.placeholder = "Type your feedback here..."
        
    }
    
    @objc func backAction() {
        self.navigationController?.popViewController(animated: true)
    }

    @IBAction func submitAction(_ sender : UIButton) {
        
        self.view.endEditing(true)
        
        let fullName = self.txtFullName.text!
        let email = self.txtEmail.text!
        let description = self.txtDescription.text!
        
        if fullName.isEmptyOrWhitespace() {
            self.showBanner(title: "Error", subTitle: "Please enter full name" , style: .danger)
            return
        }
        
        if description.isEmptyOrWhitespace() {
            self.showBanner(title: "Error", subTitle: "Please enter feedback" , style: .danger)
            return
        }

        self.startLoading(message: "")
        
        guard let uid = UserDefaults.standard.object(forKey: "userid") as? String else {
            self.showBanner(title: "Error", subTitle: "No Network connection", style: .danger)
            self.stopLoading()
            return
            
        }
        
        let values = ["email":email,"message":description ]
        Database.database().reference().child("messages").child(uid).child(UUID().uuidString).updateChildValues(values, withCompletionBlock: { (error, ref) in
            
            if let error = error
            {
                self.stopLoading()
                self.showBanner(title: "Error", subTitle: error.localizedDescription, style: .danger)
                return
            }
            self.stopLoading()
            self.txtDescription.text = ""
            self.txtDescription.placeholder = "Type your feedback here..."
            self.showBanner(title: "Success", subTitle: "Your feedback has been submitted!", style: .success)
        })
        
        DispatchQueue.main.asyncAfter(deadline: .now()+3.0)
        {
            self.stopLoading()
        }

    }
}
